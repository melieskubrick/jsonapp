# JsonApp
![ReactNative](https://img.shields.io/badge/react_native-%23007ACC.svg?style=for-the-badge&logo=react&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Axios](https://img.shields.io/badge/axios-%2307405e.svg?style=for-the-badge&logo=axios&logoColor=white)
![Expo](https://img.shields.io/badge/expo-%2307405e.svg?style=for-the-badge&logo=expo&logoColor=white)

# Doc
  - [1. Introdução](#1-introdução)
  - [2. Pré-requisitos](#2-pré-requisitos)
    - [2.1 Clonar repositório](#21-clonar-repositório)
    - [2.2 Pacotes](#22-pacotes)
  - [3. Execução](#3-execução)
  
## 1. Introdução[](url)
JsonApp é um aplicativo onde você pode listar postagens, álbums e to-do's de maneira online e offline pois o app conta com sistema de banco de dados local para armazenamento dos dados no próprio dispositivo.

## 2. Pré-requisitos

### 2.1 Clonar repositório
> Clonar o repositório [Gitlab]
```sh
   git clone https://gitlab.com/melieskubrick/jsonapp
```  
### 2.2 Pacotes
> Após clonar o projeto por completo rode os comandos abaixo
   ```sh
yarn
   ```
> Se for iOS
   ```sh
yarn
cd ios && pod install
   ```   

## 3. Execução
Para rodar a aplicação segue os comandos
   > Se deseja testar no iOS
   ```sh
   yarn ios
   ```
   ou
   ```sh
   npm run ios
   ```
   > Se deseja testar no Android
   ```sh
   yarn android
   ```
   ou
   ```sh
   npm run android
   ```

## Libs Utilizadas
Aqui estão os pacotes que foram utilizados para a construção deste app!

* [axios](https://github.com/axios/axios)
O Axios é um cliente HTTP baseado em Promises para fazer requisições
* [styled-components](https://styled-components.com/)
ES6 e CSS para estilizar a aplicação
* [lodash](https://lodash.com/)
Uma biblioteca de utilitários JavaScript moderna que oferece modularidade
* [realm](https://www.mongodb.com/docs/realm/sdk/react-native/install/)
Com o Realm React Native SDK, você pode acessar objetos armazenados em uma instância local do Realm Database

Made with ♥
