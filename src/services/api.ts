import axios, { AxiosResponse } from "axios";

import { IAlbums } from "@src/@types/albums";
import { IPosts } from "@src/@types/posts";
import { ITodos } from "@src/@types/todos";

export const api = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
});

export const getPosts = async (): Promise<AxiosResponse<IPosts | any>> => {
  try {
    const res = await api.get("/posts");
    return res;
  } catch (error) {
    if (error.response) {
      console.log("err", error);
      return error.response;
    }
    return error;
  }
};

export const getAlbums = async (): Promise<AxiosResponse<IAlbums | any>> => {
  try {
    const res = await api.get("/albums");
    return res;
  } catch (error) {
    if (error.response) {
      console.log("err", error);
      return error.response;
    }
    return error;
  }
};

export const getTodos = async (): Promise<AxiosResponse<ITodos | any>> => {
  try {
    const res = await api.get("/todos");
    return res;
  } catch (error) {
    if (error.response) {
      console.log("err", error);
      return error.response;
    }
    return error;
  }
};
