import Realm from "realm";

import { AlbumSchema } from "schemas/AlbumSchema";
import { PostSchema } from "schemas/PostSchema";
import { TodosSchema } from "schemas/TodosSchema";

export const getRealm = async () => {
  return Realm.open({
    path: "jsonapp",
    schema: [PostSchema, AlbumSchema, TodosSchema],
  });
};
