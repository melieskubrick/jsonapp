import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";

/* Screens */
import Albums from "screens/Albums";
import Home from "screens/Home";
import Posts from "screens/Posts";
import Todos from "screens/Todos";

const Stack = createNativeStackNavigator();

const MyStack: React.FC = () => {
  const renderRoutes = () => (
    <>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Posts"
          component={Posts}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Albums"
          component={Albums}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Todos"
          component={Todos}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </>
  );

  return renderRoutes();
};

export default MyStack;
