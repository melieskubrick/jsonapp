import { Badge, Text } from "@react-native-material/core";
import React from "react";

import * as S from "./styles";

interface ICard {
  title: string;
  description?: string;
  isTodo?: boolean;
  badge?: boolean;
}

const Card = ({ title, description, badge, isTodo }: ICard) => {
  return (
    <S.Container>
      <S.SurfaceView elevation={2} category="medium">
        <S.PressableView>
          {isTodo && (
            <Badge
              style={{ alignSelf: "flex-start", marginBottom: 8 }}
              label={badge ? "completo" : "incompleto"}
              color={badge ? undefined : "error"}
            />
          )}
          <Text
            variant="h6"
            style={{
              marginBottom: description && 16,
              textTransform: "uppercase",
            }}
          >
            {title}
          </Text>
          {description && <Text variant="subtitle1">{description}</Text>}
        </S.PressableView>
      </S.SurfaceView>
    </S.Container>
  );
};

export default Card;
