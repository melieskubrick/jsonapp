import { Pressable, Surface } from "@react-native-material/core";
import styled from "styled-components/native";

export const Container = styled.View``;

export const SurfaceView = styled(Surface)`
  margin: 8px 24px;
  border-radius: 10px;
`;

export const PressableView = styled(Pressable)`
  padding: 24px;
  border-radius: 10px;
`;
