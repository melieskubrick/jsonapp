export default {
  colors: {
    primary: "#202024",
    secondary: "#FFFFFF",
    black: "#000000",
    gray_light: "#8E8E8E",
    gray: "#29292E",
    gray_dark: "#121214",
    red: "#FF0000",
  },
};
