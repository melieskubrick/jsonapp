export const AlbumSchema = {
  name: "Albums",
  properties: {
    id: "int",
    userId: "int",
    title: "string",
  },
  primaryKey: "id",
};
