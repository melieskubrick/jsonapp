export const PostSchema = {
  name: "Posts",
  properties: {
    id: "int",
    userId: "int",
    title: "string",
    body: "string",
  },
  primaryKey: "id",
};
