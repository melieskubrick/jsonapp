export const TodosSchema = {
  name: "Todos",
  properties: {
    id: "int",
    userId: "int",
    title: "string",
    completed: "string",
  },
  primaryKey: "id",
};
