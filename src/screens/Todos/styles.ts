import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
`;

export const ActivityIndicatorView = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const List = styled.FlatList`
  flex: 1;
  padding-top: 8px;
`;
