import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import {
  ActivityIndicator,
  AppBar,
  HStack,
  IconButton,
} from "@react-native-material/core";
import { useNavigation } from "@react-navigation/native";
import _ from "lodash";
import React from "react";
import { getStatusBarHeight } from "react-native-status-bar-height";

import { ITodos } from "@src/@types/todos";
import Card from "components/Card";
import { getTodos } from "services/api";
import { getRealm } from "services/realm";

import * as S from "./styles";
const Todos: React.FC = () => {
  const [todos, setTodos] = React.useState<ITodos>();
  const [loading, setLoading] = React.useState<boolean>(false);

  const navigation = useNavigation();

  const saveRepository = async (todo: ITodos) => {
    const realm = await getRealm();
    const data = {
      id: todo.id,
      userId: todo.userId,
      title: todo.title,
      completed: `${todo.completed}`,
    };

    realm.write(() => {
      realm.create("Todos", data, "modified");
    });

    return data;
  };

  const fetchTodos = async () => {
    try {
      setLoading(true);
      const response = await getTodos();
      setTodos(response.data);
      response.data.map(async (todo) => await saveRepository(todo));
    } catch (e: any) {
      console.log("err::", e);
    } finally {
      setLoading(false);
    }
  };

  const fetchLocalTodos = async () => {
    const realm = await getRealm();

    const data = realm.objects("Todos");
    setTodos(data);

    if (_.isEmpty(data)) return fetchTodos();
  };

  React.useEffect(() => {
    fetchLocalTodos();
  }, []);

  return (
    <S.Container>
      <AppBar
        style={{ paddingTop: getStatusBarHeight() }}
        title="To do's"
        leading={(props) => (
          <IconButton
            onPress={() => navigation.goBack()}
            icon={(props) => <Icon name="arrow-left" {...props} />}
            {...props}
          />
        )}
        trailing={(props) => (
          <HStack>
            <IconButton
              onPress={fetchTodos}
              icon={(props) => <Icon name="refresh" {...props} />}
              {...props}
            />
          </HStack>
        )}
      />
      {loading ? (
        <S.ActivityIndicatorView>
          <ActivityIndicator size="large" />
        </S.ActivityIndicatorView>
      ) : (
        <S.List<ITodos>
          showsVerticalScrollIndicator={false}
          data={todos}
          renderItem={({ item }) => {
            let badge: boolean;

            if (typeof item.completed === "string") {
              if (item.completed === "true") {
                badge = true;
              } else {
                badge = false;
              }
            } else {
              badge = item.completed;
            }

            return <Card title={item.title} isTodo badge={badge} />;
          }}
        />
      )}
    </S.Container>
  );
};

export default Todos;
