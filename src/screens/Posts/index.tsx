import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import {
  ActivityIndicator,
  AppBar,
  HStack,
  IconButton,
} from "@react-native-material/core";
import { useNavigation } from "@react-navigation/native";
import _ from "lodash";
import React from "react";
import { getStatusBarHeight } from "react-native-status-bar-height";

import { IPosts } from "@src/@types/posts";
import Card from "components/Card";
import { getPosts } from "services/api";
import { getRealm } from "services/realm";

import * as S from "./styles";
const Posts: React.FC = () => {
  const [posts, setPosts] = React.useState<
    Realm.Results<Realm.Object> | IPosts[]
  >([]);
  const [loading, setLoading] = React.useState<boolean>(false);

  const navigation = useNavigation();

  const saveRepository = async (post: IPosts) => {
    const realm = await getRealm();
    try {
      const data = {
        id: post.id,
        userId: post.userId,
        title: post.title,
        body: post.body,
      };

      realm.write(() => {
        realm.create("Posts", data, "modified");
      });

      return data;
    } catch (e: any) {
      console.log("err::", e);
    }
  };

  const fetchPosts = async () => {
    try {
      setLoading(true);
      const response = await getPosts();
      setPosts(response.data);
      response.data.map(async (post) => await saveRepository(post));
    } catch (e: any) {
      console.log("err::", e);
    } finally {
      setLoading(false);
    }
  };

  const fetchLocalPosts = async () => {
    try {
      setLoading(true);
      const realm = await getRealm();

      const data = realm.objects("Posts");
      setPosts(data);

      if (_.isEmpty(data)) return fetchPosts();
    } catch (e: any) {
      console.log("err::", e);
    } finally {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    fetchLocalPosts();
  }, []);

  return (
    <S.Container>
      <AppBar
        style={{ paddingTop: getStatusBarHeight() }}
        title="Postagens"
        leading={(props) => (
          <IconButton
            onPress={() => navigation.goBack()}
            icon={(props) => <Icon name="arrow-left" {...props} />}
            {...props}
          />
        )}
        trailing={(props) => (
          <HStack>
            <IconButton
              onPress={fetchPosts}
              icon={(props) => <Icon name="refresh" {...props} />}
              {...props}
            />
          </HStack>
        )}
      />
      {loading ? (
        <S.ActivityIndicatorView>
          <ActivityIndicator size="large" />
        </S.ActivityIndicatorView>
      ) : (
        <S.List<IPosts>
          showsVerticalScrollIndicator={false}
          data={posts}
          renderItem={({ item }) => (
            <Card title={item.title} description={item.body} />
          )}
        />
      )}
    </S.Container>
  );
};

export default Posts;
