import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { AppBar, ListItem } from "@react-native-material/core";
import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { RootStackParamList } from "@types/RootStackParamList";
import React from "react";
import { getStatusBarHeight } from "react-native-status-bar-height";

import * as S from "./styles";
const Home: React.FC = () => {
  type navigationProp = NativeStackNavigationProp<RootStackParamList, "Home">;
  const navigation = useNavigation<navigationProp>();

  return (
    <S.Container>
      <AppBar style={{ paddingTop: getStatusBarHeight() }} title="Json App" />
      <S.List>
        <ListItem
          onPress={() => navigation.navigate("Posts")}
          title="Postagens"
          leading={<Icon name="newspaper" size={24} />}
          trailing={(props) => <Icon name="chevron-right" {...props} />}
        />
        <ListItem
          onPress={() => navigation.navigate("Albums")}
          title="Álbuns"
          leading={<Icon name="image" size={24} />}
          trailing={(props) => <Icon name="chevron-right" {...props} />}
        />
        <ListItem
          onPress={() => navigation.navigate("Todos")}
          title="To-do's"
          leading={<Icon name="pencil" size={24} />}
          trailing={(props) => <Icon name="chevron-right" {...props} />}
        />
      </S.List>
    </S.Container>
  );
};

export default Home;
