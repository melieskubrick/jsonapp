import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import {
  ActivityIndicator,
  AppBar,
  HStack,
  IconButton,
} from "@react-native-material/core";
import { useNavigation } from "@react-navigation/native";
import _ from "lodash";
import React from "react";
import { getStatusBarHeight } from "react-native-status-bar-height";

import { IAlbums } from "@src/@types/albums";
import Card from "components/Card";
import { getAlbums } from "services/api";
import { getRealm } from "services/realm";

import * as S from "./styles";

const Albums: React.FC = () => {
  const [albums, setAlbums] = React.useState<
    Realm.Results<Realm.Object> | IAlbums[]
  >([]);
  const [loading, setLoading] = React.useState<boolean>(false);

  const navigation = useNavigation();

  const saveRepository = async (album: IAlbums) => {
    const realm = await getRealm();
    const data = {
      id: album.id,
      userId: album.userId,
      title: album.title,
    };

    realm.write(() => {
      realm.create("Albums", data, "modified");
    });

    return data;
  };

  const fetchAlbums = async () => {
    try {
      setLoading(true);
      const response = await getAlbums();
      setAlbums(response.data);
      response.data.map(async (album) => await saveRepository(album));
    } catch (e: any) {
      console.log("err::", e);
    } finally {
      setLoading(false);
    }
  };

  const fetchLocalAlbums = async () => {
    const realm = await getRealm();

    const data = realm.objects("Albums");
    setAlbums(data);

    if (_.isEmpty(data)) return fetchAlbums();
  };

  React.useEffect(() => {
    fetchLocalAlbums();
  }, []);

  return (
    <S.Container>
      <AppBar
        style={{ paddingTop: getStatusBarHeight() }}
        title="Álbums"
        leading={(props) => (
          <IconButton
            onPress={() => navigation.goBack()}
            icon={(props) => <Icon name="arrow-left" {...props} />}
            {...props}
          />
        )}
        trailing={(props) => (
          <HStack>
            <IconButton
              onPress={fetchAlbums}
              icon={(props) => <Icon name="refresh" {...props} />}
              {...props}
            />
          </HStack>
        )}
      />
      {loading ? (
        <S.ActivityIndicatorView>
          <ActivityIndicator size="large" />
        </S.ActivityIndicatorView>
      ) : (
        <S.List<IAlbums>
          showsVerticalScrollIndicator={false}
          data={albums}
          renderItem={({ item }) => <Card title={item.title} />}
        />
      )}
    </S.Container>
  );
};

export default Albums;
