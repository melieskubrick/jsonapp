import { registerRootComponent } from "expo";

import App from "./src/routes";

if (__DEV__) {
  import("./reactotron").then(() => console.log("Reactotron Configured"));
}

registerRootComponent(App);
